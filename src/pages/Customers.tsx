import { FC, useState } from 'react'
import { Stack, Table, TextInput, Button } from '@mantine/core'
import { useQuery, useMutation, useQueryClient } from 'react-query'

interface ICustomer {
  id: string
  title: string
}

const projects: FC = () => {
  const [title, setTitle] = useState('')

  const queryClient = useQueryClient()

  const projectsQuery = useQuery('projects', async () => {
    const response = await fetch(
      `${import.meta.env.VITE_API_SERVER}/api/projects`,
    )

    return (await response.json()) as ICustomer[]
  })

  const projectMutation = useMutation(
    async (project: Omit<ICustomer, 'id'>) => {
      const body = JSON.stringify(project)

      await fetch(`${import.meta.env.VITE_API_SERVER}/api/projects`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*'
        },
        body: body as any,
      })
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries('projects')
      },
    },
  )

  if (projectsQuery.isLoading || !projectsQuery.data) {
    return <h1>Carregando...</h1>
  }

  return (
    <Stack>
      <div style={{ display: 'flex', alignItems: 'flex-end', gap: '8px' }}>
        <TextInput
          required
          label="Titulo"
          styles={{ root: { width: '100%' } }}
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />

        <Button
          style={{ marginTop: 10 }}
          type="submit"
          onClick={() => projectMutation.mutate({ title: title })}
        >
          Salvar
        </Button>
      </div>

      <Table highlightOnHover>
        <thead>
          <tr>
            <th>Nome</th>
          </tr>
        </thead>

        <tbody>
          {projectsQuery.data.map((project, index) => (
            <tr key={index}>
              <td>{project.title}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Stack>
  )
}

export default projects
